<?php

if (function_exists(icl_get_languages)) {
    //$languages = icl_get_languages('skip_missing=0&orderby=code');
    $languages = icl_get_languages('skip_missing=0');
    if (!empty($languages)) {
        $langs = '';
        foreach ($languages as $l) {
            if ($l['country_flag_url']) {
                if (!$l['active']) {
                    $langs .= '<li>';
                    $langs .= '<a href="' . $l['url'] . '">';
                    $langs .= '<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
                    $langs .= '</a>';
                    $langs .= '</li>';
                } else {
                    $active = '<img class="active" src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
                }
            }
        }

        echo '<div id="language_list">' . $active . '<ul>' . $langs . '</ul></div>';
    }
}
?>
