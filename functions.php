<?php
/**
 * Require Composer autoloader if installed on it's own
 */
if (file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    require_once $composer;
} else {
    define('BASE_PATH', realpath(dirname(__FILE__)));
    function my_autoloader($class)
    {
        $filename = BASE_PATH . '/lib/' . str_replace('\\', '/', $class) . '.php';
        include($filename);
    }
    spl_autoload_register('my_autoloader');
}

// set value to prod/dev for production/develop mode
// this will append minified css or expanded with maps css
define('MODE', 'dev');


define('child_template_directory', dirname(get_bloginfo('stylesheet_url')));

/**
 * Paths
 */
if (!defined('HOPPER_THEME_DIR')) {
    define('HOPPER_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
}
if (!defined('SITEURL')) {
    define("SITEURL", get_option("siteurl"));
}

$loaded = load_theme_textdomain('hopper', get_template_directory() . '/languages');

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if (! isset($content_width)) {
    $content_width = 1000; /* pixels */
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
if (! function_exists('hopper_theme_setup')) :
    function hopper_theme_setup()
    {
        add_theme_support('post-formats', array( 'aside', 'gallery' ));
        add_theme_support('post-thumbnails');
        add_theme_support('custom-background');
        add_theme_support('custom-header');
        add_theme_support('automatic-feed-links');
        add_theme_support('html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));
        add_theme_support('title-tag');

        add_theme_support('custom-background', apply_filters('hopper_custom_background_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
        )));

        register_nav_menus(array(
        'primary' => esc_html__('Primary Menu', 'hopper'),
        'header_menu' => 'Menu Header',
        'footer_menu' => 'Menu Footer',
        ));

        //set image sizes
        //add_image_size('slider', 1400, 770, true, array('center', 'center'));
        //add_image_size('small', 300, 165);
    }
endif; // hopper_theme_setup
add_action('after_setup_theme', 'hopper_theme_setup');

/*
 * Register roles, posts...
 */
function hopper_custom_init()
{
    require_once(dirname(__FILE__) . '/inc/roles.php');
    require_once(dirname(__FILE__) . '/inc/posts.php');
}
add_action('init', 'hopper_custom_init');


/*
 * Register Sidebars
 */
function custom_sidebars()
{
    
    $args = array('id' => 'main-sidebar', 'name' => __('Main sidebar', 'hopper'),);
    register_sidebar($args);
}
add_action('widgets_init', 'custom_sidebars');

/*
 * Altres imports
 */
require_once(dirname(__FILE__) . '/inc/shortcodes.php');
require_once(dirname(__FILE__) . '/inc/vc_extend.php');
require_once(dirname(__FILE__) . '/inc/widgets.php');
require_once(dirname(__FILE__) . '/inc/ajax.php');
require_once(dirname(__FILE__) . '/inc/breadcrumbs.php');
require_once(dirname(__FILE__) . '/inc/functions.php');


/**
 * Styles and scripts
 */

/**
 * Scripts: Frontend with no conditions, Add Custom Scripts to wp_head
 */
add_action('wp_enqueue_scripts', 'hopper_scripts');
function hopper_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_enqueue_script('jquery'); // Enqueue it!
        //wp_deregister_script('jquery'); // Deregister WordPress jQuery
        //wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', array(), '1.11.2');

        /**
         * Minified and concatenated scripts
         *     @vendors     plugins.min,js
         *     @custom      scripts.min.js
         *     Order is important
         */
        if (MODE=='dev') {
            wp_register_script('hopper_vendorsJs', get_template_directory_uri() . '/assets/js/vendors.js'); // Custom scripts
            wp_register_script('hopper_customJs', get_template_directory_uri() . '/assets/js/custom.js'); // Custom scripts
        }
        else{
            wp_register_script('hopper_vendorsJs', get_template_directory_uri() . '/assets/js/vendors.min.js'); // Custom scripts
            wp_register_script('hopper_customJs', get_template_directory_uri() . '/assets/js/custom.min.js'); // Custom scripts
        }
        wp_enqueue_script('hopper_vendorsJs'); // Enqueue it!
        wp_enqueue_script('hopper_customJs'); // Enqueue it!
    }

}


/**
 * Styles: Frontend with no conditions, Add Custom styles to wp_head
 */
add_action('wp_enqueue_scripts', 'hopper_styles', 100); // Add Theme Stylesheet after all other css!
function hopper_styles()
{
    /**
     * Minified and Concatenated styles
     */
    //
    
    if (MODE=='dev') {
        wp_register_style('hopper_vendor', get_template_directory_uri() . '/assets/vendor/vendor.css', array(), '1.0', 'all');
        wp_register_style('hopper_style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    } else {
        wp_register_style('hopper_vendor', get_template_directory_uri() . '/assets/vendor/vendor.min.css', array(), '1.0', 'all');
        wp_register_style('hopper_style', get_template_directory_uri() . '/style.min.css', array(), '1.0', 'all');
    }
    wp_enqueue_style('hopper_vendor');
    wp_enqueue_style('hopper_style'); // Enqueue it!

    // wp_dequeue_style( "js_composer_front" );
    // wp_deregister_style( "js_composer_front" );
}

/* remove css files! */
//dequeue css from plugins

//wp_deregister_script( 'contact-form-7' );
function hopper_dequeue_css_from_plugins()
{
    wp_dequeue_style("js_composer_front");
    wp_deregister_style("js_composer_front");
}
add_action('wp_print_styles', 'hopper_dequeue_css_from_plugins', 100);

/**
 * Comment Reply js to load only when thread_comments is active
 */
add_action('wp_enqueue_scripts', 'hopper_enqueue_comments_reply');
function hopper_enqueue_comments_reply()
{
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

/*
 * Hide some menu elements to non admin root users
 */
function hide_menu_elements()
{
    global $user_level;
    if (get_current_user_id()!=1) {
        echo '<style>
        #toplevel_page_edit-post_type-acf-field-group, #toplevel_page_vc-general{display:none;}
      </style>';
    }
}

add_action('admin_head', 'hide_menu_elements');



//secure functions
$secure = new App\SecureWp();
$secure->runActions();
