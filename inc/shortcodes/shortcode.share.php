<?php

function share($atts)
{

    ob_start();
    get_template_part('share');
    $content = ob_get_clean();
    return $content;
}

add_shortcode('share', 'share');
