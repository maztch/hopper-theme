<?php



function daterange()
{
    global $wpdb;
    static $range = array();

    if (! empty($range)) {
        return $range;
    }

    return $range = array_shift($wpdb->get_results($wpdb->prepare(
        "
        SELECT
            MIN( post_date ) AS first,
            MAX( post_date ) AS last
        FROM
            {$wpdb->posts}
        WHERE
            post_type = %s
            AND NOT post_status = 'auto-draft'
            AND NOT post_status = 'inherit'
            AND NOT post_status = 'trash'
        ",
        //is_admin() ? get_current_screen()->post_type : get_queried_object()->post_type
        'post'
    )));
}

function range_ini()
{
    $date_range = daterange();
    $date = explode('-', $date_range->first);
    //var_dump($date);
    return $date[0];
}

function range_end()
{
    $date_range = daterange();
    $date = $date_range->last;
    return $date;
}

function print_daterange()
{

    $date_range = daterange();
    printf(
        '&copy; %s &mdash; %s',
        date_i18n(
            get_option('date_format'),
            strtotime($date_range->first),
            true // GMT
        ),
        date_i18n(
            get_option('date_format'),
            strtotime($date_range->last),
            true // GMT
        )
    );
}



function get_post_siblings($limit = 3, $date = '')
{
    global $wpdb, $post;

    if (empty($date)) {
        $date = $post->post_date;
    }

    //$date = '2009-06-20 12:00:00'; // test data

    $limit = absint($limit);
    if (!$limit) {
        return;
    }

    $p = $wpdb->get_results("
    (
        SELECT 
            p1.post_title, 
            p1.post_date,
            p1.ID
        FROM 
            $wpdb->posts p1 
        WHERE 
            p1.post_date < '$date' AND 
            p1.post_type = 'post' AND 
            p1.post_status = 'publish' 
        ORDER by 
            p1.post_date DESC
        LIMIT 
            $limit
    )
    UNION 
    (
        SELECT 
            p2.post_title, 
            p2.post_date,
            p2.ID 
        FROM 
            $wpdb->posts p2 
        WHERE 
            p2.post_date > '$date' AND 
            p2.post_type = 'post' AND 
            p2.post_status = 'publish' 
        ORDER by
            p2.post_date ASC
        LIMIT 
            $limit
    ) 
    ORDER by post_date ASC
    ");
    $i = 0;
    $adjacents = array();
    for ($c = count($p); $i < $c; $i++) {
        if ($i < $limit) {
            $adjacents['prev'][] = $p[$i];
        } else {
            $adjacents['next'][] = $p[$i];
        }
    }

    $adjacents['current'] = $post;

    return $adjacents;
}

function wpml_get_url($slug)
{
    $post = get_page_by_path($slug);
    if ($post) {
        $id = icl_object_id($post->ID, 'post', true);
        $link = get_permalink($id);
    }
    return $link;
}
