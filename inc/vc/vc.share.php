<?php


add_action('vc_before_init', 'shareVC');

function sahreVC()
{
    vc_map(array(
      "name" => __("Material", "hopper"),
      "base" => "share",
      "class" => "",
      "category" => __("Content", "hopper"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Text", "hopper"),
            "param_name" => "foo",
            "value" => __("Default param value", "hopper"),
            "description" => __("Description for foo param.", "hopper")
         )
      )
    ));
}
