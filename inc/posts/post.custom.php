<?php



$labels = array(
        'name'                => _x('Custom', 'Post Type General Name', 'hopper'),
        'singular_name'       => _x('Custom', 'Post Type Singular Name', 'hopper'),
        'menu_name'           => __('Custom', 'hopper'),
        'parent_item_colon'   => __('Parent Item:', 'hopper'),
        'all_items'           => __('All Items', 'hopper'),
        'view_item'           => __('View Item', 'hopper'),
        'add_new_item'        => __('Add New Item', 'hopper'),
        'add_new'             => __('Add New', 'hopper'),
        'edit_item'           => __('Edit Item', 'hopper'),
        'update_item'         => __('Update Item', 'hopper'),
        'search_items'        => __('Search Item', 'hopper'),
        'not_found'           => __('Not found', 'hopper'),
        'not_found_in_trash'  => __('Not found in Trash', 'hopper'),
    );
    $args = array(
        'label'               => __('guies', 'hopper'),
        'description'         => __('Post Type Description', 'hopper'),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'          => array('category'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type('custom', $args);
