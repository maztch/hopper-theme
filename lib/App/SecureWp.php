<?php
namespace App;

class SecureWp
{

    public function check()
    {

    }

    
    public function remove_header_info()
    {
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'start_post_rel_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); // for WordPress >= 3.0
    }


    function at_remove_wp_ver_css_js($src)
    {
        if (strpos($src, 'ver=')) {
            $src = remove_query_arg('ver', $src);
        }
        return $src;
    }


    function fb_disable_feed()
    {
        wp_die(__('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!'));
    }

    function rbz_prevent_multisite_signup()
    {
        wp_redirect(site_url());
        die();
    }

    function remove_x_pingback($headers)
    {
        unset($headers['X-Pingback']);
        return $headers;
    }

    
    function login_errors($errors)
    {
        global $user_login;

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'lostpassword' || isset($_REQUEST['checkemail'])) {
            if (preg_match('/There is no user registered with that email address/', $errors) ||
            preg_match('/Invalid username or e-mail/', $errors) ||
            preg_match('/Check your e-mail for the confirmation link/', $errors)
            ) {
                $errors = 'If the account information you provided was valid, we have sent you an e-mail. Please check your e-mail for the confirmation link.';

                if (!isset($_REQUEST['checkemail'])) {
                    $redirect_to = 'wp-login.php?checkemail=confirm';
                    wp_safe_redirect($redirect_to);
                    exit();
                }
            }
        } else {
            if (preg_match('/password you entered for the username/', $errors) ||
             preg_match('/Invalid username/', $errors)) {
                $errors = 'Your login information was incorrect. <a href="/wp-login.php?action=lostpassword">Lost your password</a>?';
                $user_login = $_POST['log'];
                unset($_POST['log']);

                if (preg_match('/[@]/', $user_login)) {
                    $errors .= "<br><br>Hint: your email address is not your username.";
                }
            }
        }

        return $errors;
    }

    public function runActions()
    {
        // remove unnecessary header information
        add_action('init', array($this, 'remove_header_info'));

        // remove wp version meta tag and from rss feed
        add_filter('the_generator', '__return_false');

        // remove wp version param from any enqueued scripts
        add_filter('style_loader_src', array($this, 'at_remove_wp_ver_css_js'), 9999);
        add_filter('script_loader_src', array($this, 'at_remove_wp_ver_css_js'), 9999);

        // Disable ping back scanner and complete xmlrpc class.
        add_filter('wp_xmlrpc_server_class', '__return_false');
        add_filter('xmlrpc_enabled', '__return_false');

        //Remove error mesage in login
        add_filter('login_errors', create_function('$a', "return 'Invalid Input';"));

        // Remove error messages from login functions.
        // Ref : http://seclists.org/fulldisclosure/2013/Jul/38
        add_filter('login_errors', array($this, 'login_errors'));
        add_filter('login_messages', array($this, 'login_errors'));

        // remove various feeds
        add_action('do_feed', array($this, 'fb_disable_feed'), 1);
        add_action('do_feed_rdf', array($this, 'fb_disable_feed'), 1);
        add_action('do_feed_rss', array($this, 'fb_disable_feed'), 1);
        //add_action('do_feed_rss2', 'fb_disable_feed', 1);
        add_action('do_feed_atom', array($this, 'fb_disable_feed'), 1);
        add_action('do_feed_rss2_comments', array($this, 'fb_disable_feed'), 1);
        add_action('do_feed_atom_comments', array($this, 'fb_disable_feed'), 1);


        show_admin_bar(false);

        // disable redirect to login page :
        // http://wordpress.stackexchange.com/questions/85529/how-to-disable-multisite-sign-up-page
        add_action('signup_header', array($this, 'rbz_prevent_multisite_signup'));


        //remove xpingback header
        add_filter('wp_headers', array($this, 'remove_x_pingback'));
    }
}
