#Hopper theme
A base wordpress theme for Hopper


## What Can be done?
1. Live reloads browser with BrowserSync
2. CSS: Sass to CSS conversion, error catching, Autoprefixing, Sourcemaps, CSS minification, and Merge Media Queries.
3. JS: Concatenates & uglifies Vendor and Custom JS files.
4. Images: Minifies PNG, JPEG, GIF and SVG images.
5. Watches files for changes in CSS or JS
6. Watches files for changes in PHP.
7. Corrects the line endings.
8. InjectCSS instead of browser page reload.
9. Generates .pot file for i18n and l10n.
10. Import visual composer style files in order to control them
11. Ready to use composer libraries
12. Added security class for custom wp patches
13. Boostrap4 libraries


###CONFIG
- Configure the project paths in gulpfile.js (Line #23 t0 #38)
- Open the project folder in the terminal and type `npm install`  wait for the files to get downloaded. It will take about 2 minutes to add a node_modules folder inside your project.
- Once the download is complete, if you want to use visual composer just type `gulp vc-import`
- An t'hats all type `gulp` and boom!

##Commands

Things you can do:

- Run wordpress with reload `gulp`
- Process scss `gulp styles`
- Process less `gulp less`
- Process javascript `gulp cusomJs` and `gulp vendorJs`
- Run wordpress with reload `gulp`
- Compress images `gulp images`
- Generate language .pot file `gulp translate`
- Import vc images needed for styles `gulp import-vc`
- Use Code sniffer `phpcs`
- Use Code beautifier `phpcb`
- Use Code mesh detector `phpmd . text phpmd.xml`


##Prerequisite & Gotchas
- You must have Git and Node, NPM, Gulp installed globally. 
- You should never commit `node_modules` folder, you should exclude it in `.gitignore` file.
- You should need `phpcs`, `phpcbf` and `phpmd` if you wanna use them.

##Visual composer css compat.
- For vc compat styles, less files are added in order to modify the vc styles


##TODO

- Complete SecurityWp class.
- Add docs for config visual composer o theme.
- Class for manage visual composer: check if exists, set config and actions about it.

##Changelog

28/9/2016

- Added phpmd config
- Modified code sniffer settings
- Added default wordpress page-templates folder
- Autoload fallback when composer is not used
- Added SecurityWp class

27/9/2016

- better gulp processing for new files (scss, js, php...)
- added less processing for vc control
- removes vc styles
- added visual composer css load after theme styles
- added js compression
- added images optimize
- added scss processing
- reload on file changes (scss, js, php)
- added gulp tasks
- inital release