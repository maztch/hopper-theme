<?php

get_header();

?>

    <div class="content">
        <h1>WP Hopper Boilerplate!</h1>
        <p>With gulp and ready for visual composer.</p>
    </div>
    <!-- /.content -->

    <section class="content">
        <?php
        if (have_posts()) :
            while (have_posts()) :
                the_post();
            
                // Your loop code
                the_content();
            endwhile;
        else :
            echo wpautop('Sorry, no posts were found');
        endif;
        ?>
    </section><!-- .content -->

<?php get_footer(); ?>
