<?php
/**
 * The header for our theme.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>


        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-xxxxxxx-x', 'auto');
          ga('send', 'pageview');

        </script>
</head>

<body <?php body_class(); ?>>


<div class="wrap">
	<div class="header">
		<div class="logo"><a href="<?php echo bloginfo('url');?>" class="link_logo"><img src="<?php echo get_bloginfo('template_url') . '/assets/img/logo.png'?>"></a> </div>
		<?php wp_nav_menu(array('theme_location' => 'main_menu', 'container_class' => 'menu-header')); ?>

		<?php get_template_part( 'templates/languages' ); ?>
	</div>